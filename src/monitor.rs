use crate::dirscan::{read_path, FileData};
use log::{info, warn};
use snafu::{ResultExt, Snafu};
use std::path::{Path, PathBuf};
use std::sync::mpsc;
use std::thread;
use std::time::Duration;

#[derive(Debug, Snafu)]
pub enum Error {
    SendingUpdate { source: mpsc::SendError<FileData> },
    ScanningPath { source: crate::dirscan::Error },
}

pub struct Monitor {
    path: PathBuf,
    tx: mpsc::Sender<FileData>,
    refresh: Duration,
    files: Vec<FileData>,
}

impl Monitor {
    pub fn open(path: &Path, refresh: Duration) -> Result<(Self, mpsc::Receiver<FileData>), Error> {
        let (tx, rx) = mpsc::channel();
        // load initial file path
        let files = read_path(path).context(ScanningPath)?;
        // send the top (newest) one
        if !files.is_empty() {
            tx.send(files[0].clone()).context(SendingUpdate)?;
        }
        Ok((
            Self {
                refresh,
                tx,
                path: path.to_owned(),
                files,
            },
            rx,
        ))
    }

    pub fn start(self) {
        let Monitor { path, tx, refresh, mut files } = self;
        info!("Monitoring {} with {} second refresh", path.display(), refresh.as_secs());
        thread::spawn(move || loop {
            thread::sleep(refresh);
            match read_path(&path) {
                Ok(new_files) => {
                    if !new_files.is_empty() && (files.is_empty() || new_files[0].digest().as_ref() != files[0].digest().as_ref()) {
                        tx.send(new_files[0].clone()).expect("RX closed");
                    }
                    files = new_files
                }
                Err(e) => warn!("Failed to read path: {:?}", e),
            }
        });
    }
}
