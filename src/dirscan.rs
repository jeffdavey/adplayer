use log::debug;
use ring::digest::{Context, Digest, SHA256};
use snafu::{OptionExt, ResultExt, Snafu};
use std::fs::{metadata, read_dir, File};
use std::io::{BufReader, Read};
use std::path::{Path, PathBuf};
use std::time::SystemTime;

#[derive(Debug, Clone)]
pub struct FileData {
    mime_type: String,
    path: PathBuf,
    digest: Digest,
    modified: SystemTime,
}

#[derive(Debug, Snafu)]
pub enum Error {
    ScanningPath { source: std::io::Error, path: PathBuf },
    DeterminingFileType { path: PathBuf },
    BuildingDigest { source: std::io::Error, path: PathBuf },
}

impl FileData {
    pub fn digest(&self) -> Digest {
        self.digest
    }

    pub fn mime_type(&self) -> &str {
        &self.mime_type
    }

    pub fn path(&self) -> &Path {
        &self.path
    }

    pub fn modified(&self) -> SystemTime {
        self.modified
    }

    pub fn set_path(&mut self, path: &Path) {
        self.path = path.to_owned();
    }
}

fn sha256_digest(path: &Path) -> Result<Digest, Error> {
    let input = File::open(path).context(BuildingDigest { path: path.to_owned() })?;
    let mut reader = BufReader::new(input);

    let mut context = Context::new(&SHA256);
    let mut buffer = [0u8; 4096];

    loop {
        let count = reader.read(&mut buffer).context(BuildingDigest { path: path.to_owned() })?;
        if count == 0 {
            break;
        }
        context.update(&buffer[..count]);
    }

    Ok(context.finish())
}

pub fn read_path(path: &Path) -> Result<Vec<FileData>, Error> {
    debug!("Scanning {} for new files...", path.display());
    let mut files = Vec::new();
    for entry in read_dir(path).context(ScanningPath { path: path.to_owned() })? {
        let entry = entry.context(ScanningPath { path: path.to_owned() })?;
        let path = entry.path();
        let mime_type: String = tree_magic_fork::from_filepath(&path)
            .context(DeterminingFileType { path: path.to_owned() })?
            .to_string();
        if mime_type.starts_with("video") {
            debug!("Found {} type {}", &path.display(), &mime_type);
            let modified = metadata(&path)
                .context(ScanningPath { path: path.to_owned() })?
                .modified()
                .context(ScanningPath { path: path.to_owned() })?;
            let digest = sha256_digest(&path)?;
            files.push(FileData {
                modified,
                path,
                mime_type,
                digest,
            })
        } else {
            debug!("Skipping {} type {}", &path.display(), &mime_type);
        }
        files.sort_by(|a: &FileData, b| b.modified.cmp(&a.modified));
    }
    Ok(files)
}
