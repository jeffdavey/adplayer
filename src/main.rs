mod dirscan;
mod monitor;
mod play;

use monitor::Monitor;
use play::Player;
use snafu::{ResultExt, Snafu};
use snafu_cli_debug::SnafuCliDebug;
use std::path::PathBuf;
use std::time::Duration;
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "adplayer", about = "Play ads with specified player")]
struct Opt {
    #[structopt(name = "scanpath", help = "Path to scan for new video files", short = "s")]
    scan_path: PathBuf,
    #[structopt(name = "savepath", help = "Path to copy new files to", short = "p")]
    save_path: PathBuf,
    #[structopt(
        name = "playerargs",
        help = "Arguments to player",
        short = "m",
        default_value = "-fs --stop-screensaver -loop-playlist=inf --ao=null --hwdec=no"
    )]
    player_args: String,
    #[structopt(name = "player", help = "Player binary", default_value = "mpv")]
    player: String,
    #[structopt(name = "refresh", help = "Seconds before scan refresh", short = "r", default_value = "5")]
    refresh: u64,
}

#[derive(Snafu, SnafuCliDebug)]
pub enum Error {
    MonitorFailed { source: monitor::Error },
    PlayerFailed { source: play::Error },
}

fn main() -> Result<(), Error> {
    let opt = Opt::from_args();

    env_logger::from_env(env_logger::Env::default().default_filter_or("info")).init();

    let (monitor, rx) = Monitor::open(&opt.scan_path, Duration::from_secs(opt.refresh)).context(MonitorFailed)?;
    let player = Player::open(&opt.save_path, &opt.player, &opt.player_args, Duration::from_secs(opt.refresh), rx).context(PlayerFailed)?;

    monitor.start();
    player.start().context(PlayerFailed)?;
    Ok(())
}
