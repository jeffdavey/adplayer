use crate::dirscan::{read_path, FileData};
use log::{debug, info, warn};
use snafu::{OptionExt, ResultExt, Snafu};
use std::fs::{copy, remove_file};
use std::path::{Path, PathBuf};
use std::process::{Child, Command};
use std::sync::mpsc::{self, RecvTimeoutError};
use std::time::Duration;

#[derive(Debug, Snafu)]
pub enum Error {
    ScanningPath { source: crate::dirscan::Error },
    RemovingFile { source: std::io::Error, path: PathBuf },
    PlayerExiting { source: std::io::Error },
    PlayerStarting { source: std::io::Error },
    PlayerCheck { source: std::io::Error },
    CopyingFile { source: std::io::Error },
    FileName,
    ChannelClosed,
}

pub struct Player {
    rx: mpsc::Receiver<FileData>,
    player: Option<Child>,
    player_binary: String,
    player_arguments: Vec<String>,
    save_path: PathBuf,
    check_duration: Duration,
    current: Option<FileData>,
}

impl Player {
    pub fn open(path: &PathBuf, player_binary: &str, player_options: &str, refresh: Duration, rx: mpsc::Receiver<FileData>) -> Result<Self, Error> {
        let mut files = read_path(path).context(ScanningPath)?;
        let current = if !files.is_empty() {
            // delete all but the latest video file.
            files
                .drain(1..)
                .map(|file| remove_file(file.path()).context(RemovingFile { path: path.to_owned() }))
                .collect::<Result<(), Error>>()?;
            Some(files[0].clone())
        } else {
            None
        };
        let player_arguments = player_options.split(' ').map(|s| s.to_owned()).collect::<Vec<_>>();
        Ok(Self {
            rx,
            current,
            player_arguments,
            player: None,
            player_binary: player_binary.to_owned(),
            save_path: path.to_owned(),
            check_duration: refresh,
        })
    }

    pub fn handle_new_file(&mut self, old_path: Option<&Path>, mut file: FileData) -> Result<(), Error> {
        info!("Switching to new file at {}", file.path().display());
        let mut new_path = self.save_path.to_owned();
        new_path.push(file.path().file_name().context(FileName)?);
        info!("Copying {} -> {}", file.path().display(), new_path.display());
        let bytes = copy(file.path(), &new_path).context(CopyingFile)?;
        info!("Copied {} bytes. Loading file", bytes);
        file.set_path(&new_path);
        self.current = Some(file);
        self.restart_mplayer()?;
        if let Some(path) = old_path {
            let _ = remove_file(path);
        }

        Ok(())
    }

    pub fn start(mut self) -> Result<(), Error> {
        self.restart_mplayer()?;
        loop {
            match self.rx.recv_timeout(self.check_duration) {
                Ok(file) => {
                    debug!("Notified of new file: {:?}", &file);
                    if let Some(current) = self.current.take() {
                        if current.digest().as_ref() != file.digest().as_ref() {
                            if let Err(e) = self.handle_new_file(Some(current.path()), file) {
                                warn!("Ignoring new file. Failed to switch: {:?}", e);
                                self.current = Some(current);
                            }
                        } else {
                            info!("Ignoring identical new file at {}", file.path().display());
                            self.current = Some(current);
                        }
                    } else if let Err(e) = self.handle_new_file(None, file) {
                        warn!("Failed to load new file {:?}", e);
                    }
                }
                Err(e) => match e {
                    RecvTimeoutError::Disconnected => {
                        warn!("Monitor channel disconnected. Exiting");
                        return Err(Error::ChannelClosed);
                    }
                    RecvTimeoutError::Timeout => {
                        // check if we still have a child.
                        if let Some(child) = self.player.as_mut() {
                            let did_exit = child.try_wait().context(PlayerCheck)?;
                            if let Some(code) = did_exit {
                                warn!("Player exited with code {}. Restarting.", code);
                                self.restart_mplayer()?;
                            }
                        }
                    }
                },
            }
        }
    }

    fn restart_mplayer(&mut self) -> Result<(), Error> {
        if let Some(mut player) = self.player.take() {
            let _ = player.kill();
            player.wait().context(PlayerExiting)?;
        }

        if let Some(file_data) = self.current.as_ref() {
            info!("Starting player with new path {}", file_data.path().display());
            self.player = Some(
                Command::new(&self.player_binary)
                    .args(&self.player_arguments)
                    .arg(file_data.path())
                    .spawn()
                    .context(PlayerStarting)?,
            );
        } else {
            info!("Waiting for path...");
        }
        Ok(())
    }
}
